package com.example.picasso;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Image {
    @PrimaryKey
    public int id;
    public String url;

    public Image() {
    }

    @Ignore
    public Image(int id, String url) {
        this.id = id;
        this.url = url;
    }
}
