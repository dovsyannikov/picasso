package com.example.picasso;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class ImageDAO {

    @Insert
    public abstract void insertAll(List<Image> images);
    @Query("SELECT * FROM Image")
    public abstract Flowable<List<Image>> selectAll();

    @Query("DELETE FROM Image")
    public abstract void removeAll();
}
