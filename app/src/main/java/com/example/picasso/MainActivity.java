package com.example.picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.button)
    Button button;

    MyDataBase dataBase;
    CompositeDisposable disposables = new CompositeDisposable();
    Disposable disposable;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        disposable = ApiService.getCountryByName(1000)
                .map(new Function<ResponseModel, Boolean>() {
                    @Override
                    public Boolean apply(ResponseModel responseModel) throws Exception {
                        List<Image> images1 = new ArrayList<>();
                        for (int i = 0; i < responseModel.photos.size(); i++) {
                            images1.add(new Image(i+1, responseModel.photos.get(i).img_src));
                        }
                        dataBase.getImageDao().removeAll();
                        dataBase.getImageDao().insertAll(images1);
                        return true;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        /*do noting*/
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, "Smth went wrong", Toast.LENGTH_SHORT).show();
                    }
                });

        dataBase = Room.databaseBuilder(this, MyDataBase.class, "database")
                .fallbackToDestructiveMigration()
                .build();

        printCountries();
    }


    private void printCountries(){
        disposables.add(dataBase.getImageDao().selectAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Image>>() {
                    @Override
                    public void accept(final List<Image> images) throws Exception {
                        Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(imageView);
                        int i = 0;
                        imageView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                if (event.getAction() == MotionEvent.ACTION_UP){
                                    event.getRawX();
                                    event.getRawY();
                                }
                                return true;
                            }
                        });

                    }
                }));

    }
    @Override
    protected void onStop() {
        super.onStop();
        disposables.clear();
    }
}
