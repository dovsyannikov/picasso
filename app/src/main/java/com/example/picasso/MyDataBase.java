package com.example.picasso;

import android.os.Build;

import androidx.room.Database;
import androidx.room.RoomDatabase;


@Database(entities = {Image.class}, version = 1, exportSchema = false)
public abstract class MyDataBase extends RoomDatabase {
    public abstract ImageDAO getImageDao();
}
